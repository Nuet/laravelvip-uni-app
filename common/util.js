import Config from '@/core/config'

export default {

	/**
	 * 图片路径转换
	 * @param {Object} img_path
	 */
	img(img_path) {
		var path = "";
		if (img_path != undefined && img_path != "") {
			if (img_path.indexOf("http://") == -1 && img_path.indexOf("https://") == -1) {
				// path = Config.baseUrl + "/" + img_path;
				// path = this.lrw.ossHost() + img_path; // 拼接域名
				path = Config.get('ossHost') + img_path; // 拼接域名
			} else {
				path = img_path;
			}
		}
		return path;
	},
}
