import request from '@/utils/request'

// api地址
const api = {
  data: '/site/app-info'
}

// 设置项详情
export function data() {
  return request.get(api.data)
}
